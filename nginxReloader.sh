#!/bin/bash
###########

while true
do
 inotifywait --exclude .sw? -e create -e modify -e delete -e move /etc/nginx/conf.d
 # wait for seltype to be set correctly
 sleep 1
 nginx -t
 if [ $? -eq 0 ]
 then
  echo "Detected Nginx Configuration Change"
  echo "Executing: nginx -s reload"
  nginx -s reload
 fi
done