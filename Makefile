image:= jlcox1970/openresty-reload
build:
	podman build -t $(image) .
buildx:
	docker buildx build --platform=linux/amd64,linux/arm64 -t $(image) . --push
push: build
	podman push $(image)	
